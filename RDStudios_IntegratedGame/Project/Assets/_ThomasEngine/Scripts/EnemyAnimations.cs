﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimations : MonoBehaviour
{

    [SerializeField]
    private Animator anim;

    [SerializeField]
    private string animSpeed = "speed";

    [SerializeField]
    private string animGrounded = "grounded";
    [SerializeField]
    private string animStunned = "stunned";

    [SerializeField]
    private string animHurt = "hurt";
    [SerializeField]
    private string animDead = "dead";

    public bool grounded;
    public bool dead;
    public BoolWrapper hurt = new BoolWrapper (false);
    public bool stunned;

    public float speed;

    void Update ()
    {
        if (anim)
            SyncAnimations ();
    }

    void SyncAnimations ()
    {
        anim.SetBool (animGrounded, grounded);
        anim.SetBool (animDead, dead);
        anim.SetBool (animHurt, hurt.Value);
        anim.SetBool (animStunned, stunned);

        anim.SetFloat (animSpeed, speed);
    }

    public void BoolSwitch (BoolWrapper _boolWrapper, bool _switchOn, int _frameCount)
    {
        StartCoroutine (StartBoolSwitch (_boolWrapper, _switchOn, _frameCount));
    }

    IEnumerator StartBoolSwitch (BoolWrapper _boolWrapper, bool _switchOn, int _frameCount)
    {
        _boolWrapper.Value = _switchOn;
        int i = 0;
        while (i < _frameCount)
        {
            i++;
            yield return new WaitForEndOfFrame ();
        }
        _boolWrapper.Value = !_switchOn;
    }

}