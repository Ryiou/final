﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Player))]
public class PlayerController : MonoBehaviour
{

    public enum JumpStyle { Consistent, Additive }

    public enum MovementState { Idle, Walking, Running, Crouching, InAir, Climbing }
    private MovementState movementState;
    public MovementState CurMovementState { get { return movementState; } }

    //Movement
    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private bool enableRun = true;
    [SerializeField]
    private float runSpeed = 15;
    private BoolWrapper run = new BoolWrapper(false);
    [SerializeField]
    private bool enableCrouch = true;
    [SerializeField]
    private float crouchSpeed = 2;
    [SerializeField]
    private float crouchSpeedTime = 1;
    private BoolWrapper crouch = new BoolWrapper(false);
    private bool crouching;
    [SerializeField]
    private bool enableClimbing = true;
    [SerializeField]
    private float climbSpeed = 6;
    public bool climbing;
    private bool onLadder;
    public bool IsOnLadder { get { return onLadder; } set { onLadder = value; } }
    private float curSpeed;
    [SerializeField]
    private float inAirControlTime = 1;
    private bool controlFading;

    //Jumping
    [SerializeField]
    private JumpStyle jumpStyle = JumpStyle.Additive;
    [SerializeField]
    private float jumpPower = 6;
    private bool jump;
    private bool doubleJumping;
    [SerializeField]
    private bool enableDoubleJump;
    private bool doubleJumpActive;
    [SerializeField]
    private bool enableJumpClimbing = true;
    private BoolWrapper jumpClimbing = new BoolWrapper(false);
    [SerializeField]
    private bool faceRightAtStart = true;
    [SerializeField]
    private float gravityMultiplier;
    [SerializeField]
    private float lowJumpMultiplier;

    //Ground Detection
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    public Vector2 groundBoxSize = Vector2.zero;
    [SerializeField]
    public Vector2 groundBoxCenter = Vector2.zero;
    private bool grounded;
    public bool IsGrounded { get { return grounded; } }
    private Collider2D[] groundHits;
    private bool onPlatform;
    private GameObject currentGroundGO;

    //Ceiling Detection
    [SerializeField]
    private LayerMask ceilingMask;
    [SerializeField]
    public Vector2 ceilingBoxSize = Vector2.zero;
    [SerializeField]
    public Vector2 ceilingBoxCenter = Vector2.zero;
    [SerializeField] private Vector2 curCeilingBoxCenter;
    private Vector2 crouchCeilingBoxCenter;
    private Collider2D[] ceilingHits;
    private BoolWrapper ceilingHit = new BoolWrapper(false);
    private Collider2D curCeilingHit;
    private float lastCeilingYPos;

    //Wall Detection
    [SerializeField]
    private bool enableWallJump = true;
    [SerializeField]
    private LayerMask wallMask;
    [SerializeField]
    private Vector2 leftDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 leftDetectCenter = Vector2.left;
    [SerializeField]
    private Vector2 rightDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 rightDetectCenter = Vector2.right;
    [SerializeField]
    private bool autoFlip = true;
    [SerializeField]
    private float wallBounceForceX = 5;
    [SerializeField]
    private float wallBounceForceY = 5;
    private bool wallHitLeft;
    private bool wallHitRight;
    [SerializeField]
    private bool disableMovementOnWallJump = true;
    [SerializeField]
    private float disableTime = 1;

    //movement inputs
    private float inputHor;
    private float inputVer;
    private Vector2 inputDirection;
    private Vector2 move;

    //comps
    private Player pl;
    private CapsuleCollider2D conCollider;
    private Rigidbody2D rb;
    private PlayerAnimations anim;

    private bool facingRight = true;

    private Vector2 startColOffset;
    private Vector2 startColSize;

    private BoolWrapper disableInput = new BoolWrapper(false);

    private float lastYPos;
    private float curYPos;

    private bool bouncing;

    private void Start()
    {
        InitializeController();
    }

    private void Update()
    {
        GetInputs();
        CheckDirection();
        SetMovementStates();
        CheckMovementStates();
        CheckBouncing();
        CheckKillHeight();
        SyncAnimations();
    }

    private void FixedUpdate()
    {
        CheckCeiling();
        CheckGrounded();
        CheckWallHits();
        CheckHeightVelocity();
        Move();
    }

    void InitializeController()
    {
        //set start speed
        curSpeed = speed;
        //set Direction
        if (!faceRightAtStart)
            FlipController();

        curCeilingBoxCenter = ceilingBoxCenter;
        crouchCeilingBoxCenter = new Vector2(ceilingBoxCenter.x, ceilingBoxCenter.y / 2);

        //get collider
        conCollider = GetComponent<CapsuleCollider2D>();
        startColSize = conCollider.size;
        startColOffset = conCollider.offset;

        //get comps
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<PlayerAnimations>();
        pl = GetComponent<Player>();
    }

    void GetInputs()
    {
        if (pl.IsDead())
            return;

        if (disableInput.Value)
        {
            inputHor = 0;
            inputVer = 0;
            run.Value = false;
            return;
        }

        //movement axis input
        inputHor = Input.GetAxisRaw("Horizontal");
        inputVer = Input.GetAxisRaw("Vertical");
        inputDirection = new Vector2(inputHor, inputVer).normalized;

        //jump input
        jump = Input.GetButtonDown("Jump");
        if (jump)
            Jump();

        //run input
        if (Input.GetButton("Run") || Input.GetAxisRaw("XboxTriggerLeft") > 0)
            run.Value = true;
        else
            run.Value = false;

        //crouch input
        
        if (inputDirection.y < -0.69f && !climbing)
            crouch.Value = true;
        else
            crouch.Value = false;
    }

    void CheckMovementStates()
    {
        if (onLadder && enableClimbing && inputDirection.y > 0.69f && !jumpClimbing.Value)
        {
            movementState = MovementState.Climbing;
        }
        else if (climbing)
            movementState = MovementState.Climbing;
        else if (crouching && ceilingHit.Value || crouch.Value && enableCrouch)
            movementState = MovementState.Crouching;
        else if (grounded)
        {
            if (Mathf.Abs(inputHor) > 0)
            {
                if (run.Value && enableRun)
                    movementState = MovementState.Running;
                else
                    movementState = MovementState.Walking;
            }
            else
                movementState = MovementState.Idle;
        }
        else
        {
                movementState = MovementState.InAir;
        }
            
    }

    void SetMovementStates()
    {
        switch (movementState)
        {
            case MovementState.Idle:
                Idle();
                break;
            case MovementState.Walking:
                Walking();
                break;
            case MovementState.Running:
                Running();
                break;
            case MovementState.Crouching:
                Crouching();
                break;
            case MovementState.InAir:
                InAir();
                break;
            case MovementState.Climbing:
                Climbing();
                break;
        }
    }

    void Idle()
    {
        curSpeed = speed;
    }

    void Walking()
    {
        curSpeed = speed;
    }

    void Running()
    {
        curSpeed = runSpeed;
    }

    void Crouching()
    {
        if (!crouch.Value && !ceilingHit.Value)
        {
            ResetCrouch();
            return;
        }

        if (crouching)
            return;

        Crouch(new Vector2(conCollider.offset.x, startColOffset.y / 2), new Vector2(conCollider.size.x, startColSize.y / 2));

        if (curSpeed > crouchSpeed && crouchSpeedTime > 0)
            StartCoroutine(StartSpeedFade(curSpeed, crouchSpeed, crouchSpeedTime, crouch));
        else
            curSpeed = crouchSpeed;

        crouching = true;

    }

    void Crouch(Vector2 _offset, Vector2 _size)
    {
        conCollider.offset = _offset;
        conCollider.size = _size;
    }

    void ResetCrouch()
    {
        Crouch(startColOffset, startColSize);
        crouching = false;
    }

    void InAir()
    {
        if (Mathf.Abs(inputHor) > 0)
        {
            if (controlFading)
                return;

            StartCoroutine(StartControlFade(inputHor, inAirControlTime));
        }
    }

    void Climbing()
    {
        if (!onLadder)
        {
            StopClimbing();
            return;
        }

        if (jump && enableJumpClimbing && !jumpClimbing.Value)
        {
            StartCoroutine(StartClimbJump());
            StopClimbing();
            return;
        }

        if (jumpClimbing.Value)
            return;

        if (inputVer == 0)//freeze y position so there is no slow fall
            rb.constraints = (RigidbodyConstraints2D)6;
        else//unfreeze y position but reset velocity
        {
            rb.velocity = Vector2.zero;
            rb.constraints = (RigidbodyConstraints2D)4;
        }

        if (climbing)
            return;

        curSpeed = climbSpeed;

        if (!facingRight)
            FlipController();

        climbing = true;
    }

    void StopClimbing()
    {
        rb.constraints = (RigidbodyConstraints2D)4;
        climbing = false;
    }

    void Move()
    {
        if (pl.IsDead())
            return;

        if (disableInput.Value)
            return;

        if (!controlFading)
        {
            move = transform.right * inputHor;
            if (climbing)
                move = new Vector2(inputHor, inputVer).normalized;
        }
            
        transform.Translate(move * curSpeed * Time.deltaTime);

    }

    void Jump()
    {
        if (!grounded && !doubleJumping && enableDoubleJump)
        {
            //enable double jumping until grounded/climbing
            StartCoroutine(StartJumpSwitch());
            doubleJumpActive = true;
        }

        if (grounded || doubleJumpActive || wallHitLeft || wallHitRight || climbing)
        {
            Vector2 force = Vector2.up * jumpPower;
            if (!grounded)
            {
                if (wallHitLeft || wallHitRight)
                {
                    if (!enableWallJump)
                        return;

                    if (wallHitLeft)
                    {
                        if (!facingRight)
                        {
                            force = new Vector2(wallBounceForceX, jumpPower + wallBounceForceY);
                            if (autoFlip)
                                FlipController();
                        }
                        else
                            force = Vector2.zero;
                    }
                    else if (wallHitRight)
                    {
                        if (facingRight)
                        {
                            force = new Vector2(-wallBounceForceX, jumpPower + wallBounceForceY);
                            if (autoFlip)
                                FlipController();
                        }
                        else
                            force = Vector2.zero;
                    }

                    if (disableMovementOnWallJump)
                        StartCoroutine(StartSpeedFade(0, curSpeed, disableTime, new BoolWrapper(!grounded)));
                }
            }

            //jump player
            rb.velocity = force;

            //set double jump
            if (enableDoubleJump)
            {
                if (doubleJumpActive)
                    doubleJumpActive = false;
            }

        }

    }

    void CheckGrounded()
    {

        curYPos = Mathf.Round(transform.position.y * 100) / 100;

        if (curYPos != lastYPos || onPlatform)
        {
            //ground detect
            groundHits = Physics2D.OverlapBoxAll((Vector2)transform.position + groundBoxCenter, groundBoxSize, 0, groundMask);
            if (groundHits.Length > 0)
            {
                currentGroundGO = groundHits[0].gameObject;
                grounded = true;

                if (currentGroundGO.layer == LayerMask.NameToLayer("Platform"))
                {
                    if (!onPlatform)
                        StickToPlatform(currentGroundGO.transform, true);
                }
                else if (onPlatform)
                    StickToPlatform(null, false);
            }
            else if (onPlatform)
            {
                StickToPlatform(null, false);
            }
            else
                grounded = false;
        }

        lastYPos = Mathf.Round(transform.position.y * 100) / 100;

    }

    void CheckCeiling()
    {
        if (crouching)
        {
            if (curCeilingBoxCenter != crouchCeilingBoxCenter)
                curCeilingBoxCenter = crouchCeilingBoxCenter;
        }
        else if (curCeilingBoxCenter != ceilingBoxCenter)
            curCeilingBoxCenter = ceilingBoxCenter;

        ceilingHits = Physics2D.OverlapBoxAll((Vector2)transform.position + curCeilingBoxCenter, ceilingBoxSize, 0, ceilingMask);
        if (ceilingHits.Length > 0)
        {
            ceilingHit.Value = true;

            if (ceilingHits[0] != curCeilingHit)
            {
                curCeilingHit = ceilingHits[0];
                lastCeilingYPos = curCeilingHit.transform.position.y;
                return;
            }

            if (curCeilingHit.transform.position.y < lastCeilingYPos)//is ceiling moving down?
                pl.KillPlayer("being crushed by " + ceilingHits[0].ToString());

            lastCeilingYPos = curCeilingHit.transform.position.y;

        }
        else if (ceilingHit.Value)
            ceilingHit.Value = false;
    }

    void CheckBouncing()
    {
        if (grounded && bouncing || jump)
            bouncing = false;
    }

    void StickToPlatform(Transform _platform, bool _stick)
    {
        onPlatform = _stick;
        transform.SetParent(_platform);
    }

    void CheckWallHits()
    {
        //left detect        
        wallHitLeft = Physics2D.OverlapBox((Vector2)transform.position + leftDetectCenter, leftDetectSize, 0, wallMask);
        //right detect
        wallHitRight = Physics2D.OverlapBox((Vector2)transform.position + rightDetectCenter, rightDetectSize, 0, wallMask);
    }

    void CheckHeightVelocity()
    {
        if (climbing || jumpStyle == JumpStyle.Consistent || bouncing)
            return;

        if (rb.velocity.y < 0)
            AddGravity(gravityMultiplier);
        if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            AddGravity(lowJumpMultiplier);
    }

    void AddGravity(float _multiplier)
    {
        rb.velocity += Vector2.up * Physics2D.gravity.y * _multiplier * Time.deltaTime;
    }

    void CheckDirection()
    {
        if (climbing)
            return;

        //flip controller based on input left or right
        if (inputHor < 0 && facingRight || inputHor > 0 && !facingRight)
        {
            FlipController();
        }
    }

    void CheckKillHeight()
    {
        if (transform.position.y < GameManager.instance.GetKillHeight())
        {
            pl.KillPlayer("Falling below the GameManager's kill height");
        }
    }

    void RunTimer(float _time, FloatWrapper _timer, FloatWrapper _perc)
    {
        _timer.Value += Time.deltaTime;
        if (_timer.Value > _time)
            _timer.Value = _time;
        _perc.Value = _timer.Value / _time;
    }

    IEnumerator StartControlFade(float _startHor, float _time)
    {
        FloatWrapper timer = new FloatWrapper(0);
        FloatWrapper perc = new FloatWrapper(0);
        controlFading = true;
        while (perc.Value < 1 && !grounded && !climbing)
        {
            RunTimer(_time, timer, perc);
            float hor = Mathf.Lerp(_startHor, inputHor, perc.Value);
            move = transform.right * hor;
            yield return new WaitForEndOfFrame();
        }
        controlFading = false;
    }

    IEnumerator StartSpeedFade(float _startSpeed, float _endSpeed, float _time, BoolWrapper _condition)
    {
        float timer = 0;
        float perc = 0;
        while (perc < 1 && _condition.Value)
        {
            timer += Time.deltaTime;
            if (timer > _time)
                timer = _time;
            perc = timer / _time;
            curSpeed = Mathf.Lerp(_startSpeed, _endSpeed, perc);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator StartBoolTimer(BoolWrapper _bool, float _time)
    {
        float timer = 0;
        float perc = 0;
        _bool.Value = true;
        while (perc < 1)
        {
            timer += Time.deltaTime;
            if (timer > _time)
                timer = _time;
            perc = timer / _time;
            yield return new WaitForEndOfFrame();
        }
        _bool.Value = false;
    }

    IEnumerator StartJumpSwitch()
    {
        //anim switch
        if (anim)
            anim.BoolSwitch(anim.doubleJumpSwitch, true, 1);

        //possible to do a jump out of the air if an input jump was not the cause for being not grounded.
        doubleJumping = true;
        while (!grounded && !climbing)
            yield return new WaitForEndOfFrame();
        doubleJumping = false;
    }

    IEnumerator StartClimbJump()
    {
        jumpClimbing.Value = true;
        while (rb.velocity.y > 0)
        {
            yield return new WaitForEndOfFrame();
        }
        jumpClimbing.Value = false;
    }

    void FlipController()
    {
        transform.Rotate(0, 180, 0);
        facingRight = !facingRight;
    }

    public void Bounce(Vector2 _direction, float _force)
    {
        bouncing = true;
        rb.velocity = _direction * _force;
    }

    public void StunDisableMovement(float _time)
    {
        StartCoroutine(StartBoolTimer(disableInput, _time));
    }

    public void SetSpeed(float _newSpeed, float _time)
    {
        StartCoroutine(StartSpeedFade(curSpeed, _newSpeed, _time, new BoolWrapper(true)));
    }

    public void DisableInput(bool _disable)
    {
        disableInput.Value = _disable;
    }

    void SyncAnimations()
    {
        if (!anim)
            return;

        anim.facingRight = facingRight;
        anim.grounded = grounded;
        anim.running = run.Value;
        anim.crouching = crouching;
        anim.climbing = climbing;
        anim.jump = jump;
        anim.wallHitLeft = wallHitLeft;
        anim.wallHitRight = wallHitRight;
        anim.inputHor = inputHor;
        anim.inputVer = inputVer;
    }

}
