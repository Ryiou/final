﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour 
{

    [SerializeField]
    private SpawnManager manager;

    private int checkPointInd;

	void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {
            manager.SetCurCheckPoint(checkPointInd);
        }
    }

    public void SetCheckPointInd(int _ind)
    {
        checkPointInd = _ind;
    }
}
