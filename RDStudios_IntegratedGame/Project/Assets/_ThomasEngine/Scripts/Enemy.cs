﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyController))]
[RequireComponent(typeof(Collider2D))]
public class Enemy : MonoBehaviour 
{

    public enum DeathMovement { StopIfGrounded, FallThrough }

    [SerializeField]
    private int maxHp = 5;
    private int curHp;

    [SerializeField]
    private bool disableMovementIfStunned;
    [SerializeField]
    private float stunTime = 1;
    private bool stunned;
    public bool IsStunned() { return stunned; }
    [SerializeField]
    private float deathTime = 1;
    [SerializeField]
    private GameObject deathFX;

    [SerializeField]
    private DeathMovement deathMovement;
    [SerializeField]
    private Behaviour[] disableOnDeath;

    private EnemySoundFX soundFX;

    private bool dead = false;

    private EnemyController con;
    private Collider2D col;
    private EnemyAnimations anim;


    // Use this for initialization
    void Start () 
	{
        curHp = maxHp;

        //getcomp
        soundFX = GetComponent<EnemySoundFX>();
        col = GetComponent<Collider2D>();
        con = GetComponent<EnemyController>();
        anim = GetComponent<EnemyAnimations>();
	}

    public void AddHp(int _amount)
    {
        //only add health if not at max
        if (curHp < maxHp)
        {
            curHp += _amount;           
        }

    }

    public void DamageHp(int _damage)
    {
        if (dead)
            return;

        curHp -= _damage;

        //kill player if health at 0
        if (curHp <= 0)
        {
            curHp = 0;
            StartCoroutine(StartDeath());
        }

        //play sound
        if (soundFX)
            soundFX.PlayHurtSound();

        //anim
        if (anim)
            anim.BoolSwitch(anim.hurt, true, 30);

        //stun
        if (disableMovementIfStunned)
            StartCoroutine(StartStun());
        
    }

    IEnumerator StartDeath()
    {
        dead = true;

        DisableComponents();

        //play sound
        if (soundFX)
            soundFX.PlayDeathSound();

        //anim
        if (anim)
            anim.dead = true;

        Vector2 lastPos = transform.position;
        float timer = 0;
        while (timer < deathTime)
        {
            timer += Time.deltaTime;
            if (timer > deathTime)
                timer = deathTime;
            if (deathMovement == DeathMovement.StopIfGrounded)
            {
                if (con.IsGrounded)
                    transform.position = lastPos;
                else
                    lastPos = transform.position;
            }
            yield return new WaitForFixedUpdate();
        }
    
        DestroyEnemy();
    }

    IEnumerator StartStun()
    {
        stunned = true;
        //anim sync
        anim.stunned = stunned;
        float timer = 0;
        while (timer < stunTime)
        {
            timer += Time.deltaTime;
            if (timer > stunTime)
                timer = stunTime;
            yield return new WaitForEndOfFrame();
        }
        stunned = false;
        //anim sync
        anim.stunned = stunned;
    }

    void DisableComponents()
    {
        //always disable collider
        col.enabled = false;

        //optional disable other components
        foreach (var item in disableOnDeath)
        {
            item.enabled = false;
        }
    }

    void DoDeathFX()
    {
        if (deathFX)
            Instantiate(deathFX, transform.position, Quaternion.LookRotation(Vector3.up));
    }

    void DestroyEnemy()
    {
        DoDeathFX();
        Destroy(this.gameObject);
    }

    public int GetCurHp()
    {
        return curHp;
    }

    public bool isDead()
    {
        return dead;
    }


}
