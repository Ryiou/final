﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [SerializeField]
    private int maxHp = 5;
    [SerializeField]
    private int maxLives = 3;
    [SerializeField]
    private bool allowGameOver = false;
    [SerializeField]
    private Transform enemyTarget;
    [SerializeField]
    private List<GameObject> skins = new List<GameObject>();

    private int curHp;
    private int curLives;
    public int CurLives { get { return curLives; } }
    [SerializeField]
    private bool setSkin;
    [SerializeField]
    private int skinInd;
    [SerializeField]
    private bool ignorePhysicsOnHit = true;
    [SerializeField]
    private int ignoreLayerOne = 11;
    [SerializeField]
    private int ignoreLayerTwo = 17;
    [SerializeField]
    private float ignoreTime = 1;
    [SerializeField]
    private float stunTime = 1;
    private bool physicsIgnored;
    [SerializeField]
    private bool changeMeshMaterialOnHit;
    private Renderer[] meshesToChange;
    private Material[] startMeshesMaterials;
    [SerializeField]
    private Material materialToUse;
    private int curSkinInd;

    private bool dead;

    private PlayerUI ui;
    private GameDataManager dataManager;
    private SpawnManager sm;
    private PlayerAnimations anim;
    private PlayerSoundFX soundFX;
    private PlayerController con;

    // Use this for initialization
    void Start()
    {
        //get components
        ui = GameManager.instance.GetPlayerUI();
        dataManager = GameManager.instance.GetGameDataManager();
        sm = GameManager.instance.GetSpawnManager();
        con = GetComponent<PlayerController>();
        anim = GetComponent<PlayerAnimations>();

        StartCoroutine(StartWaitForData());
        
    }

    IEnumerator StartWaitForData()
    {
        while (dataManager.GetCurPlayer() == null)
        {
            yield return new WaitForEndOfFrame();
        }
        curLives = maxLives;
        //save lives to disc
        dataManager.SetCurLives(curLives);

        //set player skin from disc
        if (setSkin)
            SetPlayerSkin(skinInd);
        else
            SetPlayerSkin(dataManager.GetCurPlayerSkin());

        //get sound
        soundFX = GetCurSkin().GetComponent<PlayerSoundFX>();

        //set playeravatar
        PlayerSkin skin = GetCurSkin().GetComponent<PlayerSkin>();
        if (skin)
            ui.SetPlayerAvater(skin.GetSkinAvater());

        SetupPlayer();

    }

    public void IgnorePhysicsLayers()
    {
        StartCoroutine( StartIgnorePhysicsLayers());
    }

    IEnumerator StartIgnorePhysicsLayers()
    {
        physicsIgnored = true;
        SetHurtMeshMaterials(true);
        Physics2D.IgnoreLayerCollision(ignoreLayerOne,ignoreLayerTwo, true);
        float timer = 0;
        while (timer < ignoreTime)
        {
            timer += Time.deltaTime;
            if (timer > ignoreTime)
                timer = ignoreTime;

            yield return new WaitForEndOfFrame();
        }
        physicsIgnored = false;
        SetHurtMeshMaterials(false);
        Physics2D.IgnoreLayerCollision(ignoreLayerOne, ignoreLayerTwo, false);

    }

    void GetMaterials()
    {
        if (!changeMeshMaterialOnHit)
            return;

        meshesToChange = GetCurSkin().GetComponentsInChildren<Renderer>();
        if (meshesToChange.Length > 0)
        {
            startMeshesMaterials = new Material[meshesToChange.Length];
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                startMeshesMaterials[i] = meshesToChange[i].material;
            }
        }
        else
            Debug.LogError("No Meshrenderers found on " + GetCurSkin());

    }

    void SetHurtMeshMaterials(bool _hurt)
    {
        if (!changeMeshMaterialOnHit)
            return;

        if (_hurt)
        {
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                meshesToChange[i].material = materialToUse;
            }
        }
        else
        {
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                meshesToChange[i].material = startMeshesMaterials[i];
            }
        }

    }

    public void SetPlayerSkin(int _ind)
    {
        curSkinInd = _ind;
        dataManager.SetCurPlayerSkin(curSkinInd);

        foreach (var skin in skins)
        {
            if (skin != skins[curSkinInd])
            {
                skin.SetActive(false);
            }
            else
            {
                skin.SetActive(true);
                anim.SetAnimationController(skin.GetComponent<Animator>());
            }
        }
    }

    public void SetupPlayer()
    {
        curHp = maxHp;

        //set UI stats
        if (ui)
        {
            ui.AddHp(curHp);
            ui.SetLives(curLives);
        }

        dead = false;

        //update anim
        anim.SetDead(false);

        //for ignore physics
        GetMaterials();

    }

    public void AddHp(int _amount)
    {
        //only add health if not at max
        if (curHp < maxHp)
        {
            curHp += _amount;
            //update ui
            if (ui)
            {
                ui.AddHp(_amount);
            }
            
        }

    }

    public void DamageHp(int _damage)
    {
        ApplyDamage(_damage, 0, Vector2.zero, false);
    }

    public void DamageHp(int _damage, float _bounceForce, Vector2 _direction, bool _stunMovement)
    {
        ApplyDamage(_damage, _bounceForce, _direction, _stunMovement);
    }

    void ApplyDamage(int _damage, float _bounceForce, Vector2 _direction, bool _stunMovement)
    {
        if (dead) //dont apply damage if fade is in effect or dead
            return;

        if (physicsIgnored)
            return;

        //bounce player
        if (_bounceForce > 0)
            BouncePlayer(_bounceForce, _direction);

        //stun player movement if grounded
        if (con.IsGrounded && _stunMovement)
            con.StunDisableMovement(stunTime);

        //play hurt sound
        if (soundFX)
            soundFX.PlayHurtSound();

        //sync anim for one frame
        if (anim)
            anim.BoolSwitch(anim.hurt, true, 1);

        curHp -= _damage;
        //kill player if health at 0
        if (curHp <= 0)
        {
            curHp = 0;
            KillPlayer("no health");
        }
        else if (ignorePhysicsOnHit)
            //ignore physics
            IgnorePhysicsLayers();

        //update UI
        if (ui)
        {
            ui.RemoveHp(_damage);
        }

        Debug.Log("damaging player by " + _damage + " at " + _direction + " direction.");
        
    }

    void BouncePlayer(float _bounceForce, Vector2 _direction)
    {
        con.Bounce(_direction, _bounceForce);
    }

    public void KillPlayer(string _reason)
    {
        if (dead)
            return;

        dead = true;
            
        //update anim
        if (anim)
            anim.SetDead(true);

        if (curHp > 0)
        {
            if (ui)//update UI
                ui.RemoveHp(curHp);

            curHp = 0;
        }
            
        curLives--;
        if (curLives > 0)
            Respawn();
        else
        {
            //dont respawn
            if (allowGameOver)
            {
                curLives = 0;
                ui.SetLives(curLives);
            }
            else //force reset respawn...no gameover
            {
                curLives = maxLives;
                Respawn();
            }

            
        }
        
        Debug.Log(gameObject + " has died from " + _reason + "!");


        //play death sound
        if (soundFX)
            soundFX.PlayDeathSound();

    }

    public void Respawn()
    {
        sm.RespawnPlayer();
    }

    public GameObject GetCurSkin()
    {
        return skins[curSkinInd];
    }

    public int GetCurSkinInd()
    {
        return curSkinInd;
    }

    public int GetCurSkinCount()
    {
        return skins.Count;
    }

    public int GetCurHp()
    {
        return curHp;
    }

    public int GetCurLives()
    {
        return curLives;
    }

    public Transform GetEnemyTarget()
    {
        return enemyTarget;
    }

    public bool IsDead()
    {
        return dead;
    }

    public bool IsPhysicsIgnored()
    {
        return physicsIgnored;
    }
}
