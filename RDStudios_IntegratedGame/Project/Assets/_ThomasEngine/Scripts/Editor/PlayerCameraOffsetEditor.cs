﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(PlayerCameraOffset))]
public class PlayerCameraOffsetEditor : Editor
{

    private SerializedObject sourceRef;

    private SerializedProperty triggerTag;
    private SerializedProperty playerCamera;
    private SerializedProperty offsetType;
    private SerializedProperty disableDelay;
    private SerializedProperty newCamPos;
    private SerializedProperty offset;
    private SerializedProperty offsetTime;
    private SerializedProperty disableTime;
    private SerializedProperty resetOffsetOnExit;
    private SerializedProperty resetTime;
    private SerializedProperty smooth;

    private void OnEnable()
    {
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {

        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    void GetProperties()
    {
        triggerTag = sourceRef.FindProperty("triggerTag");
        playerCamera = sourceRef.FindProperty("playerCamera");
        offsetType = sourceRef.FindProperty("offsetType");
        disableDelay = sourceRef.FindProperty("lockDelay");
        newCamPos = sourceRef.FindProperty("newCamPos");
        offset = sourceRef.FindProperty("newOffset");
        offsetTime = sourceRef.FindProperty("offsetTime");
        disableTime = sourceRef.FindProperty("lockTime");
        resetOffsetOnExit = sourceRef.FindProperty("resetOffsetOnExit");
        resetTime = sourceRef.FindProperty("resetTime");
        smooth = sourceRef.FindProperty("smooth");
    }

    void SetProperties()
    {
        triggerTag.stringValue = EditorGUILayout.TagField("Trigger Tag", triggerTag.stringValue);
        EditorGUILayout.PropertyField(playerCamera);

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(offsetType);
        if (offsetType.enumValueIndex == 1)
        {
            EditorGUILayout.PropertyField(newCamPos);
            EditorGUILayout.PropertyField(disableDelay);
            EditorGUILayout.PropertyField(disableTime);
            EditorGUILayout.PropertyField(resetTime);
            resetOffsetOnExit.boolValue = false;
        }
        else
        {
            EditorGUILayout.PropertyField(offset);
            EditorGUILayout.PropertyField(resetOffsetOnExit);
            if (resetOffsetOnExit.boolValue)
                EditorGUILayout.PropertyField(resetTime);
        }
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(offsetTime);
        EditorGUILayout.PropertyField(smooth);
    }

}
