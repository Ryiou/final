﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerAnimations))]
public class PlayerAnimationsEditor : Editor
{
    private SerializedObject sourceRef;

    //Animation Sync
    private SerializedProperty anim;
    private SerializedProperty forceIdle;
    private SerializedProperty animIdleStateName;
    //movement
    private SerializedProperty animFacingRight;
    private SerializedProperty animInputHor;
    private SerializedProperty animInputVer;
    private SerializedProperty animRun;
    private SerializedProperty animCrouch;
    private SerializedProperty animClimbing;
    private SerializedProperty animGrounded;
    //Jumping
    private SerializedProperty animJump;
    private SerializedProperty animDoubleJump;
    private SerializedProperty animWallHitLeft;
    private SerializedProperty animWallHitRight;
    //Health
    private SerializedProperty animHurt;
    private SerializedProperty animDead;

    //foldouts
    protected static bool showMovement = true;
    protected static bool showJumping = true;
    protected static bool showHealth = true;

    private void OnEnable()
    {
        sourceRef = serializedObject;

        GetAnimationProperties();
    }

    public override void OnInspectorGUI()
    {
        SetAnimationProperties();

        sourceRef.ApplyModifiedProperties();
    }

    void GetAnimationProperties()
    {
        anim = sourceRef.FindProperty("anim");
        forceIdle = sourceRef.FindProperty("forceIdle");
        animIdleStateName = sourceRef.FindProperty("animIdleStateName");
        //movement
        animFacingRight = sourceRef.FindProperty("animFacingRight");
        animInputHor = sourceRef.FindProperty("animInputHor");
        animInputVer = sourceRef.FindProperty("animInputVer");
        animRun = sourceRef.FindProperty("animRun");
        animCrouch = sourceRef.FindProperty("animCrouch");
        animClimbing = sourceRef.FindProperty("animClimbing");
        animGrounded = sourceRef.FindProperty("animGrounded");
        //jumping
        animJump = sourceRef.FindProperty("animJump");
        animDoubleJump = sourceRef.FindProperty("animDoubleJump");
        animWallHitLeft = sourceRef.FindProperty("animWallHitLeft");
        animWallHitRight = sourceRef.FindProperty("animWallHitRight");
        //health
        animHurt = sourceRef.FindProperty("animHurt");
        animDead = sourceRef.FindProperty("animDead");
    }

    void SetAnimationProperties()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("The Animator to sync");
        EditorGUILayout.PropertyField(anim);
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Only use idle? Mostly used while in menus");
        EditorGUILayout.PropertyField(forceIdle);
        EditorGUILayout.Space();

        if (forceIdle.boolValue)
        {
            EditorGUILayout.PropertyField(animIdleStateName);
            return;
        }
            

        EditorGUILayout.LabelField("Type out the names of the controller parameters");

        showMovement = EditorGUILayout.Foldout(showMovement, "Movement");
        if (showMovement)
        {
            EditorGUILayout.PropertyField(animFacingRight);
            EditorGUILayout.PropertyField(animInputHor);
            EditorGUILayout.PropertyField(animInputVer);
            EditorGUILayout.PropertyField(animRun);
            EditorGUILayout.PropertyField(animCrouch);
            EditorGUILayout.PropertyField(animClimbing);
            EditorGUILayout.PropertyField(animGrounded);
            
        }
        showJumping = EditorGUILayout.Foldout(showJumping, "Jumping");
        if (showJumping)
        {
            EditorGUILayout.PropertyField(animJump);
            EditorGUILayout.PropertyField(animDoubleJump);
            EditorGUILayout.PropertyField(animWallHitLeft);
            EditorGUILayout.PropertyField(animWallHitRight);
        }
        showHealth = EditorGUILayout.Foldout(showHealth, "Health");
        if (showHealth)
        {
            EditorGUILayout.PropertyField(animHurt);
            EditorGUILayout.PropertyField(animDead);
        }

    }
}
