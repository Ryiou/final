﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Enemy))]
public class EnemyController : MonoBehaviour 
{
    public enum EnemyMovementType {Static, Walking}

    [SerializeField]
    private bool faceRightAtStart = true;
    [SerializeField]
    private EnemyMovementType movementType = EnemyMovementType.Walking;
    [SerializeField]
    private float speed = 3;
    [SerializeField]
    private bool useBoundaries = true;
    public Vector2 leftPos;
    public Vector2 rightPos;

    [SerializeField]
    private bool flipOnCollision = true;
    [SerializeField]
    private LayerMask collisionMask;
    [SerializeField]
    private Vector2 leftDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 leftDetectCenter = Vector2.left;
    [SerializeField]
    private Vector2 rightDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 rightDetectCenter = Vector2.right;
    private Collider2D[] leftCols;
    private Collider2D[] rightCols;

    [SerializeField]
    private bool bounce;
    [SerializeField]
    private float bouncePower = 5;
    [SerializeField]
    private float bounceXAddedForce;
    [SerializeField]
    private float bounceDelay;
    private float bounceTimer;

    //Ground Detection
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    public Vector2 groundBoxSize = Vector2.zero;
    [SerializeField]
    public Vector2 groundBoxCenter = Vector2.zero;
    private bool grounded;
    public bool IsGrounded { get { return grounded; } }

    private EnemySoundFX soundFX;

    private bool facingRight = true;

    private Rigidbody2D rb;
    private Enemy enemy;

    private bool pausePatrol;

    private float curSpeed;
    private Vector3 lastPos;
    private float lastYPos;
    private float curYPos;

    private Bounds bounds;

	// Use this for initialization
	void Start () 
	{

        InititalizeController();
        GetComponents();
        StartCoroutine(CalcSpeed());
    }

	// Update is called once per frame
	void FixedUpdate () 
	{
        if (pausePatrol || enemy.isDead())
            return;

        CheckGrounded();
        CheckWallHits();
        CheckBoundaries();
        MoveEnemy();

        if (bounce)
        {
            BounceEnemy();
        }

    }

    void GetComponents()
    {
        //get components
        rb = GetComponent<Rigidbody2D>();
        soundFX = GetComponent<EnemySoundFX>();
        enemy = GetComponent<Enemy>();
        bounds = GetComponent<Collider2D>().bounds;
    }

    void InititalizeController()
    {
        //make sure rotations are consistent
        transform.rotation = Quaternion.identity;
        if (!faceRightAtStart)
            FlipController();
    }

    public void BounceEnemy(Vector2 _direction, float _bounceForce)
    {
        rb.AddForce(_direction * _bounceForce, ForceMode2D.Impulse);
    }

    void MoveEnemy()
    {
        if (enemy.IsStunned())
            return;

        if (movementType == EnemyMovementType.Walking)
            //move enemy..forward
            transform.Translate(transform.right * speed * Time.deltaTime, Space.World);
        //move enemy sideways in air if xforce and bounce enabled
        else if (movementType == EnemyMovementType.Static && bounce && !grounded && bounceXAddedForce > 0)
            transform.Translate(transform.right * bounceXAddedForce * Time.deltaTime, Space.World);
    }

    void CheckBoundaries()
    {
        if (!useBoundaries)
            return;

        //rotate enemy based in waypoint x position
        if ((transform.position.x - bounds.extents.x) <= leftPos.x && !facingRight)
        {
            FlipController();
        }
        else if ((transform.position.x + bounds.extents.x) >= rightPos.x && facingRight)
        {
            FlipController();
        }
    }

    void FlipController(int _frameDelay = default(int))
    {
        if (_frameDelay == 0)
        {
            facingRight = !facingRight;
            transform.Rotate(0, 180, 0);
        }
        else
        {
            StartCoroutine(StartFlipController(_frameDelay));
        }

    }

    IEnumerator StartFlipController(int _frameDelay)
    {
        int i = 0;
        while (i < _frameDelay)
        {
            i++;
            yield return new WaitForFixedUpdate();
        }
        facingRight = !facingRight;
        transform.Rotate(0, 180, 0);
    }

    void CheckGrounded()
    {
        curYPos = Mathf.Round(transform.position.y * 100) / 100;

        if (curYPos != lastYPos) // only check ground if controller's y position changes..save some fps
        { 
            //ground detect
            grounded = Physics2D.OverlapBox((Vector2)transform.position + groundBoxCenter, groundBoxSize, 0, groundMask);
        }
        
        lastYPos = Mathf.Round(transform.position.y * 100) / 100; ;
    }

    void CheckWallHits()
    {
        if (!flipOnCollision)
            return;

        //left detect        
        leftCols = Physics2D.OverlapBoxAll((Vector2)transform.position + leftDetectCenter, leftDetectSize, 0, collisionMask);
        if (leftCols.Length > 0)
        {
            GetCollisionInformation(leftCols, false);
        }

        //right detect      
        rightCols = Physics2D.OverlapBoxAll((Vector2)transform.position + rightDetectCenter, rightDetectSize, 0, collisionMask);
        if (rightCols.Length > 0)
        {
            GetCollisionInformation(rightCols, true);
        }
    }

    void GetCollisionInformation(Collider2D[] _cols, bool _right)
    {
        foreach (var col in _cols)
        {
            //only flip if the collider does not equal this one
            if (col.gameObject != gameObject)
            {
                if (facingRight == _right)
                {
                    if (col.gameObject.layer == gameObject.layer)//if same layer, wait a frame for other to collide as well
                        FlipController(1);
                    else
                        FlipController();
                }

            }

        }
    }

    void BounceEnemy()
    {
        if (enemy.IsStunned())
            return;

        if (grounded)
        {
            bounceTimer += Time.deltaTime;
            if (bounceTimer > bounceDelay)
            {
                rb.Sleep();

                rb.AddForce(transform.up * bouncePower, ForceMode2D.Impulse);

                //playsound
                if (soundFX)
                    soundFX.PlayAttackSound();

                bounceTimer = 0;
            }

        }    
    }

    public void PausePatrol(bool _pause)
    {
        if (!_pause)
        {
            //make sure character is facing the correct direction after unpausing
            if (transform.localRotation.y == 0)
                facingRight = true;
            else
                facingRight = false;
        }
        pausePatrol = _pause;
    }

    public float GetMaxSpeed()
    {
        return speed;
    }

    public float GetCurSpeed()
    {
        return curSpeed;
    }

    IEnumerator CalcSpeed()
    {
        while (Application.isPlaying)
        {
            // Position at frame start
            lastPos = transform.position;
            // Wait till it the end of the frame
            yield return new WaitForFixedUpdate();
            // Calculate velocity: Velocity = DeltaPosition / DeltaTime
            curSpeed = ((lastPos - transform.position) / Time.fixedDeltaTime).magnitude;
        }
    }
}
