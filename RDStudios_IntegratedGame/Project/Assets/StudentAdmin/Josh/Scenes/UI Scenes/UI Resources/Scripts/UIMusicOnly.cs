﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIMusicOnly : MonoBehaviour
{

    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("UImusic");
        if (objs.Length > 1)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "Level1Final")
        {
            Destroy(this.gameObject);
        }
        {
            if (SceneManager.GetActiveScene().name == "Level2Final")
            {
                Destroy(this.gameObject);
            }
            {
                if (SceneManager.GetActiveScene().name == "Level3Final")
                {
                    Destroy(this.gameObject);
                }
                {
                    if (SceneManager.GetActiveScene().name == "Level4Final")
                    {
                        Destroy(this.gameObject);
                    }
                    {
                        if (SceneManager.GetActiveScene().name == "Level5FinalRoom1")
                        {
                            Destroy(this.gameObject);
                        }
                        {
                            if (SceneManager.GetActiveScene().name == "Level5FinalRoom2")
                            {
                                Destroy(this.gameObject);
                            }
                        }
                        {
                            if (SceneManager.GetActiveScene().name == "Level5FinalRoom3")
                            {
                                Destroy(this.gameObject);
                            }
                        }
                    }
                }
            }
        }
    }
}
